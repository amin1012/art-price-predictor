FROM python:3.10-slim

ENV WORKDIR="/app"

COPY . $WORKDIR
WORKDIR $WORKDIR

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 libgl1 -y
RUN pip install poetry
RUN poetry install -n --no-ansi --only main

CMD ["poetry", "run", "uvicorn", "api:app", "--host", "0.0.0.0", "--port", "8080"]
