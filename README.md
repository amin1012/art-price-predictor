# How ro run localy?
```bash
poetry install --only main
poetry run uvicorn api:app
```

# How to run in Docker?
```bash
docker build --tag art_price_predictor ./
docker run --network="host" art_price_predictor
```
