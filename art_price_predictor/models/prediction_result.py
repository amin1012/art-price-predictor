from pydantic import BaseModel


# TODO: write fields
class PredictionResult(BaseModel):
    name: str
    price: int
    currency: str = "USD"
