import cv2

from art_price_predictor.processing.predict import (
    get_model_predict,
    process_price,
    transform_image,
)
from art_price_predictor.settings import MODEL_PATH


def test_predict():
    image = cv2.imread("art_price_predictor/data/images/image_509.png")
    transformed_image = transform_image(image)
    price = get_model_predict(MODEL_PATH, transformed_image)
    result = process_price(price=price, lower_bound=0, upper_bound=1_000_000)

    assert (result >= 0) and (result <= 1_000_000)
