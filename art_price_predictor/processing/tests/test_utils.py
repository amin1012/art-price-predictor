from art_price_predictor.processing.utils import is_image_file


def test_is_image_file():
    assert is_image_file("some.png")
    assert is_image_file("some.jpg")
    assert is_image_file("some.jpeg")
    assert is_image_file("some.PNG")

    assert is_image_file("some.txt") is False
    assert is_image_file("some") is False
