import cv2
import numpy as np
import onnxruntime

from art_price_predictor.models import Image, PredictionResult
from art_price_predictor.processing.utils import is_image_file
from art_price_predictor.settings import MODEL_PATH


def predict(file: Image) -> PredictionResult:
    """
    Обработчик для получения предсказания стоимости картины
    :param file:
    :return:
    """
    if not is_image_file(file.filename):
        return PredictionResult(name=file.filename, price=0)

    raw_image: np.ndarray = np.asarray(bytearray(file.file), dtype="uint8")
    decoded_image: np.ndarray = cv2.imdecode(raw_image, cv2.IMREAD_COLOR)
    transformed_image = transform_image(decoded_image)

    price: float = get_model_predict(MODEL_PATH, transformed_image)
    processed_price: np.ndarray = process_price(price, 0, 100_000)

    result: PredictionResult = PredictionResult(
        name=file.filename, price=processed_price
    )

    return result


def transform_image(image: np.ndarray) -> np.ndarray:
    """
    Преобразование изображения для входа в модель
    :param image: входное изображение
    :return:
    """
    cv_image: np.ndarray = cv2.resize(image, (228, 228))

    transformed_image: np.ndarray = (
        cv_image[np.newaxis, :, :, :].transpose(0, 3, 1, 2).astype("float32") / 255.0
    )
    return transformed_image


def process_price(price: float, lower_bound: float, upper_bound: float) -> np.ndarray:
    """
    Обработка предикта модели
    :param price: сырой выход модели
    :param lower_bound: нижняя граница цены
    :param upper_bound: верхняя граница цены
    :return:
    """
    clipped_price: np.ndarray = np.clip(np.exp(price), lower_bound, upper_bound)
    return clipped_price


def get_model_predict(model_path: str, image: np.ndarray) -> float:
    """
    Функция для получения предсказания модели
    :param model_path: путь до onnx-модели
    :param image: подготовленое изображение
    :return:
    """
    model = onnxruntime.InferenceSession(model_path)
    model_input: dict = {model.get_inputs()[0].name: image}
    model_output: np.ndarray = model.run(None, model_input)
    model_result: float = model_output[0][0][0]
    return model_result
