def is_image_file(filename: str) -> bool:
    """
    Проверка на то, что файл с расширением изображения
    :param filename: имя файла
    :return: bool
    """
    return any(
        filename.lower().endswith(extention) for extention in [".png", ".jpg", ".jpeg"]
    )
