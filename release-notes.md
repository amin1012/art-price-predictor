# Art Price Predictor Release Notes

## 0.5.0
* CI from another repo

## 0.4.0
* CI added

## 0.3.1
* Bug fixes

## 0.3.0
* File uploading added

## 0.2.0
* Default metrics added

## 0.1.1
* Deploy bug fixed, another small fixes

## 0.1.0
* Init version
