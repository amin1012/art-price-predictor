import typing

from fastapi import FastAPI, UploadFile, Response
from prometheus_fastapi_instrumentator import Instrumentator

from art_price_predictor.models import Image, PredictionResult
from art_price_predictor.processing.predict import predict
from art_price_predictor.monitoring import instrumentator
from starlette_exporter import PrometheusMiddleware, handle_metrics


app = FastAPI(docs_url="/")

instrumentator.instrument(app).expose(app, include_in_schema=False, should_gzip=True)

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)


@app.post("/predict")
def prediction_api(file: UploadFile, filename: typing.Optional[str] = None) -> Response:
    name: str = file.filename
    if filename is not None:
        name = filename

    image: Image = Image(
        filename=name,
        file=file.file.read()
    )

    result: PredictionResult = predict(image)
    response: Response = Response(
        headers={"X-PRICE": str(result.price)}, 
        content=result.json()
    )

    return response
